FROM php:7.3-apache

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install --fix-missing -y libpq-dev
RUN apt-get install --no-install-recommends -y libpq-dev
RUN apt-get install -y libxml2-dev libbz2-dev zlib1g-dev
RUN apt-get -y install libsqlite3-dev libsqlite3-0 mariadb-client curl exif ftp vim
RUN docker-php-ext-install intl
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN apt-get -y install --fix-missing zip unzip
RUN apt-get -y install --fix-missing git

# Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer self-update --2
 
RUN a2enmod rewrite

RUN sed -i "s/AllowOverride None/AllowOverride all/" /etc/apache2/apache2.conf

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN cd /var/www/html

RUN composer create-project codeigniter4/framework DropDev

Copy . /var/www/html/DropDev

RUN /usr/local/bin/php DropDev/spark migrate

RUN sed -ie 's/DocumentRoot\ \/var\/www\/html/DocumentRoot\ \/var\/www\/html\/DropDev\/public/g' /etc/apache2/sites-available/000-default.conf


RUN apache2ctl configtest

CMD service apache2 reload

CMD service apache2 restart


CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
