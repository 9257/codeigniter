<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'InstallAppController::access');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
$routes->add('installApp', 'InstallAppController::access');
// $routes->add('findNeighbour', 'Welcome::findNeighbour');
$routes->add('fetchNeighboursByLatLong', 'Welcome::fetchNeighboursByLatLong');



// $route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;

/*
* @Author: Mitesh Patel
*/
$routes->add('dashboard/(:any)', 'Dropd::index/$1');
$routes->add('auth', 'InstallAppController::auth');
$routes->add('redirect', 'InstallAppController::authCallback');
$routes->add('settings/(:any)', 'Dropd::settings/$1');
$routes->add('settings_save', 'Dropd::settingsSubmit');
$routes->add('charges/(:any)', 'Dropd::getCharges/$1');
$routes->add('orders/(:any)', 'Dropd::getOrders/$1');
$routes->add('create_orders', 'Dropd::createOrder');
$routes->add('fulfill_orders', 'Dropd::fulfillOrder');
$routes->add('approve_charge', 'InstallAppController::approveRecurringCharge');
$routes->add('recur_accept/(:any)', 'Dropd::recurAccept/$1');
$routes->add('get_productid', 'Dropd::getProductID');
$routes->add('get_variantid', 'Dropd::getVariantID');
$routes->add('usage_charge/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Dropd::addUsageCharge/$1/$2/$3/$4/$5/$6');

$routes->add('uninstall', 'InstallAppController::uninstall');
$routes->add('order_status', 'Dropd::orderStatus');
