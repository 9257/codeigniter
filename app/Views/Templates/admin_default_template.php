<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<!-- <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/public/images/favicon.png"/> -->
	<title>DropD</title>
	<link href="<?php echo base_url(); ?>/public/admin/css/styles.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/public/admin/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/public/admin/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/public/admin/css/bootstrap-datepicker.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/public/admin/css/custom.css?v=<?php echo time(); ?>" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>/public/admin/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body class="sb-nav-fixed my_body sb-sidenav-toggled" data-base="<?php echo base_url(); ?>">
<div id="cover-spin"></div>
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
	<a class="navbar-brand" href="<?php echo base_url().'/dashboard/'.base64_encode($id); ?>">DropD</a>
	<!-- <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button> -->
		
		<ul>
			<li>
				<a class="nav-link <?= (uri_string() === 'settings/'.base64_encode($id)) ? 'active' : ''; ?>" href="<?php echo base_url().'/settings/'.base64_encode($id); ?>">
					<div class="sb-nav-link-icon"><i class="fa fa-cog"></i></div>
					Settings
				</a>
			</li>
			<li>
				<a class="nav-link <?= (uri_string() === 'charges/'.base64_encode($id)) ? 'active' : ''; ?>" href="<?php echo base_url().'/charges/'.base64_encode($id); ?>">
					<div class="sb-nav-link-icon"><i class="fas fa-coins"></i></div>
					Usage Charges
				</a>
			</li>
			<li>
				<a class="nav-link <?= (uri_string() === 'orders/'.base64_encode($id)) ? 'active' : ''; ?>" href="<?php echo base_url().'/orders/'.base64_encode($id); ?>">
					<div class="sb-nav-link-icon"><i class="fas fa-list"></i></div>
					Orders
				</a>
			</li>
		</ul>
	
</nav>
<div id="layoutSidenav">
	<!-- <div id="layoutSidenav_nav">
		<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
			<div class="sb-sidenav-menu">
				<div class="nav">
					<div class="sb-sidenav-menu-heading"></div>
					
					<a class="nav-link" href="<?php echo base_url().'/settings/'.base64_encode($id); ?>">
						<div class="sb-nav-link-icon"><i class="fa fa-cog"></i></div>
						Settings
					</a>
					<a class="nav-link" href="<?php echo base_url().'/charges/'.base64_encode($id); ?>">
						<div class="sb-nav-link-icon"><i class="fas fa-coins"></i></div>
						Usage Charges
					</a>
					<a class="nav-link" href="<?php echo base_url().'/orders/'.base64_encode($id); ?>">
						<div class="sb-nav-link-icon"><i class="fas fa-list"></i></div>
						Orders
					</a>
				</div>
			</div>

		</nav>
	</div> -->
	<div id="layoutSidenav_content">
		<?php if(isset($is_recur_approve)){ 
          	if($is_recur_approve == 0){
          ?>
          <div id="sub_modal" class="modal fade show" aria-modal="true" style="padding-right: 17px; display: block;" data-url="<?php echo $confirmation_url; ?>">
          	<div class="modal-dialog">
              	<div class="modal-content">
                  	<div class="modal-body">
                      <p>You need to approve the application recurring charges for use this site.</p>
                      <p class="text-warning"><small>Click on Accept button to accept charges.</small></p>
                  	</div>
                  	<div class="modal-footer">
                      <a type="button" target="_blank" href="<?php echo $confirmation_url; ?>"  class="btn btn-primary" >Accept</a>
                  	</div>

              	</div>
          	</div>
        </div>
        <div class="modal-backdrop fade show"></div>
    	<?php } } ?>
		<?= $this->renderSection('content') ?>
		<footer class="py-4 bg-light mt-auto">
			<div class="container-fluid">
				<div class="d-flex align-items-center justify-content-between small">
				</div>
			</div>
		</footer>
	</div>
</div>
<script src="<?php echo base_url(); ?>/public/admin/js/jquery-3.5.1.min.js" ></script>
<script src="<?php echo base_url(); ?>/public/admin/js/bootstrap.min.js" ></script>
<script src="<?php echo base_url(); ?>/public/admin/js/scripts.js?v=<?php echo time(); ?>" ></script>
<script src="<?php echo base_url(); ?>/public/admin/js/jquery.validate.min.js" ></script>
<script src="<?php echo base_url(); ?>/public/admin/js/jquery.dataTables.min.js" ></script>
<script src="<?php echo base_url(); ?>/public/admin/js/dataTables.bootstrap4.min.js" ></script>
<script src="<?php echo base_url(); ?>/public/admin/js/bootstrap-datepicker.js" ></script>
<script src="<?php echo base_url(); ?>/public/admin/js/custom.js?v=<?php echo time(); ?>" ></script>
</body>
</html>
