<?= $this->extend('Templates/admin_default_template') ?>
<?= $this->section('content') ?>
<main>
	<div class="container-fluid">
		<h1 class="mt-4">Welcome</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Welcome</li>
		</ol>
		<div class="row">
			<div class="col-xl-12 col-md-12">
				<div class="card bg-primary text-white mb-4">
					<div class="card-body">Hello, You are successfully logged in application.</div>
					<div class="card-footer d-flex align-items-center justify-content-between">
						You can manage orders charges, settings etc.
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?= $this->endSection() ?>

