<?= $this->extend('Templates/admin_default_template') ?>
<?= $this->section('content') ?>
<main>
	<div class="container-fluid">
		<h1 class="mt-4">Orders</h1>
		 <ol class="breadcrumb mb-4">
			<!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li> -->
			<li class="breadcrumb-item active">Orders</li>
		</ol>
		<?php
		if (session()->getFlashdata('success') !== NULL) {
			echo '<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ' .  session()->getFlashdata('success') . '
				</div>';
		}
		if (session()->getFlashdata('error') !== NULL) {
			echo '<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ' .  session()->getFlashdata('error') . '
				</div>';
		}
		?>
		<div class="card mb-4">
			<div class="card-header">
				<i class="fas fa-list mr-1"></i>
				Orders
			</div>

			<div class="card-body">
				<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Orders</th>									
									<th>Book Neighboar Success</th>
									<th>Shipping to merchant Success</th>									
									<th>Created</th>									
								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($orders)){
									foreach ($orders as $o_key => $order) {
								?>
								<tr>
									<td><?php echo $order['order_id']; ?></td>
									<td><?php 
									if($order['book_neighbour_success'] == 1){
										echo "Yes";
									} else {
										echo "No";
									}
									?></td>
									<td><?php 
									if($order['shipping_to_merchant_success'] == 1){
										echo "Yes";
									} else {
										echo "No";
									}
									?></td>
									<td><?php echo date('M d, Y H:i',strtotime($order['created']));  ?></td>
								</tr>
								<?php }  } else { ?>
								<tr>                                                                  
									<td colspan="5" align="center" width="100%">No records found.</td>
								</tr>                                                                 
								<?php } ?>
							</tbody>
						</table>
				</div>
			</div>
		</div>
	</div>
</main>
<?= $this->endSection() ?>
