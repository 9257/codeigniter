<?= $this->extend('Templates/admin_default_template') ?>
<?= $this->section('content') ?>
<main>
	<div class="container-fluid">
		<h1 class="mt-4">Settings</h1>
		<ol class="breadcrumb mb-4">
			<li class="breadcrumb-item active">Settings</li>
		</ol>
		<?php
		if (session()->getFlashdata('success') !== NULL) {
			echo '<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ' .  session()->getFlashdata('success') . '
				</div>';
		}
		if (session()->getFlashdata('error') !== NULL) {
			echo '<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ' .  session()->getFlashdata('error') . '
				</div>';
		}
		?>
		<div class="container">
		  <form method="post" action="<?php echo base_url().'/settings_save' ?>">
		  	<input type="hidden" name="client_id" value="<?php echo $id; ?>">
		  	<input type="hidden" name="settings_id" value="<?php if(!empty($settings)){ if($settings['id'] != ''){ echo $settings['id']; }} ?>">
		  	<input type="hidden" name="product_id" value="<?php if(!empty($settings)){ if($settings['product_id'] != ''){ echo $settings['product_id']; }} ?>">
		    <div class="form-group">
		      <label for="customer_id">Customer ID:</label>
		      <input type="text" class="form-control" id="customer_id" placeholder="Enter Customer ID" name="customer_id" value="<?php if(!empty($settings)){ if($settings['customer_id'] != ''){ echo $settings['customer_id']; }} ?>">
		    </div>
		    <div class="form-group">
		      <label for="service_charge">Service Charge</label>
		      <input type="number" class="form-control" id="service_charge" placeholder="Enter Service Charge" name="service_charge" value="<?php if(!empty($settings)){ if($settings['service_charge'] != ''){ echo $settings['service_charge']; }} ?>">
		    </div>
		    <button type="submit" class="btn btn-primary">Save</button>
		  </form>
		</div>
	</div>
</main>
<?= $this->endSection() ?>

