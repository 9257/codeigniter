<style type="text/css">
	.button {
  background-color: blue; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  border-radius: 10px;
}

</style>
<?php
$url = '';
if(!empty($data)){
	$url = "https://".$data['shop'].'/admin/orders/'.$data['id'];
}
?>
<main>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12 col-md-12">
				<div class="card bg-primary text-white mb-4">
					<a target="_blank" href="<?php echo $url; ?>" class="button">Go back</a>
				</div>
			</div>
		</div>
	</div>
</main>

