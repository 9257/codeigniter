<?= $this->extend('Templates/admin_default_template') ?>
<?= $this->section('content') ?>
<main>
	<div class="container-fluid">
		<h1 class="mt-4">Usage Charges</h1>
		<ol class="breadcrumb mb-4">
			<!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li> -->
			<li class="breadcrumb-item active">Usage Charges</li>
		</ol>
		<?php
		if (session()->getFlashdata('success') !== NULL) {
			echo '<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ' .  session()->getFlashdata('success') . '
				</div>';
		}
		if (session()->getFlashdata('error') !== NULL) {
			echo '<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ' .  session()->getFlashdata('error') . '
				</div>';
		}
		?>
		<div class="card mb-4">
			<div class="card-header">
				<i class="fas fa-list mr-1"></i>
				Usage Charges
			</div>

			<div class="card-body">
				<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Charge Id</th>
					                <th>Description</th>
					                <th>Price</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($all_charges)){
									foreach ($all_charges as $charge_key => $charge) {
										
								?>
								<tr>
									<td><?php echo $charge['usage_charge_id']; ?></td>
									<td><?php echo $charge['description'];  ?></td>
									<td><?php echo $charge['price']; ?></td>
									
								</tr>
							<?php }  } else { ?>
								<tr>                                                                  
									<td colspan="6" align="center" width="100%">No records found.</td>
								</tr>                                                                 
							<?php } ?>
							</tbody>
						</table>
				</div>
			</div>
		</div>
	</div>
</main>
<?= $this->endSection() ?>

