<?php
namespace App\Models;

use CodeIgniter\Model;

class WebhooksModel extends Model
{
    protected $table      = 'webhooks';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['client_id', 'webhook_id', 'topic', 'address', 'response', 'is_active', 'created', 'modified'];

    protected $useTimestamps = false;

    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>