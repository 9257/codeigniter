<?php
namespace App\Models;

use CodeIgniter\Model;

class RecurringApplicationChargeModel extends Model
{
    protected $table      = 'recurring_application_charge';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['client_id', 'recurring_application_charge_id', 'client_id', 'name', 'price', 'response', 'is_approve', 'is_active', 'created', 'modified'];

    protected $useTimestamps = false;

    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>