<?php 

class Common_Model extends CI_Model {

	public function insert($table,$data)
	{
		$this->db->insert($table,$data);
	}

	public function getAllData($table)
	{
		$result = $this->db->get($table);
		return $result->result_array();
	}

	public function getSingleData($table,$where)
	{
		$this->db->where($where);
		$res = $this->db->get($table);
		return $res->row_array();
	}

	public function deleteData($table,$where)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
	public function find_all($table_name, $keys, $cons = null, $sort = null)
	{
		$this->db->select($keys);
		if(!empty($cons)){
			foreach ($cons as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		if(!empty($sort)){
			foreach ($sort as $s_key => $s_value) {
				$this->db->order_by($s_key, $s_value);
			}
		}
		$data = $this->db->get($table_name)->result_array();
		return $data;
	}

	public function find_by($table_name, $keys, $cons = null, $sort = null)
	{
		$this->db->select($keys);
		if(!empty($cons)){
			foreach ($cons as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		if(!empty($sort)){
			foreach ($sort as $s_key => $s_value) {
				$this->db->order_by($s_key, $s_value);
			}
		}
		$data = $this->db->get($table_name)->row_array();
		return $data;
	}

	public function add_data($table_name, $data)
	{
		$c_data = array();
		foreach ($data as $key => $value) {
			/*
			*@Author: Mitesh
			*/
			if($value != null || $value != ''){
				$value = addStripSlashes($value);
			}
			/* End code */
			$c_data[$key] = $value;
		}
		$this->db->insert($table_name, $c_data);
        return $this->db->insert_id();
	}

	public function update_data($table_name, $data, $con)
	{
		foreach ($data as $key => $value) {
			if($key != 'id'){
				/*
				*@Author: Mitesh
				*/
				if($value != null || $value != ''){
					$value = addStripSlashes($value);
				}
				/* End code */
				$this->db->set($key, $value);
			}
		}
		foreach ($con as $c_key => $c_value) {
			$this->db->where($c_key, $c_value);
		}
		
		return $this->db->update($table_name);
	}

	public function find_first($table_name, $keys, $con = null, $order = null)
	{
		$this->db->select($keys);
		if(!empty($cons)){
			foreach ($cons as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		if(!empty($sort)){
			foreach ($sort as $s_key => $s_value) {
				$this->db->order_by($s_key, $s_value);
			}
		}
		$data = $this->db->get($table_name)->row_array();
		return $data;
	}


	public function delete_data($table_name, $data, $con)
	{
		foreach ($data as $key => $value) {
			if($key != 'id'){
				$this->db->set($key, $value);
			}
		}
		foreach ($con as $c_key => $c_value) {
			$this->db->where($c_key, $c_value);
		}
		
		return $this->db->delete($table_name);
	}
}
?>