<?php
namespace App\Models;

use CodeIgniter\Model;

class UsageChargeModel extends Model
{
    protected $table      = 'usage_charge';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['client_id', 'usage_charge_id', 'description', 'price', 'created_at', 'billing_on', 'balance_used', 'balance_remaining', 'response', 'risk_level', 'is_active', 'created', 'modified'];

    protected $useTimestamps = false;

    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>