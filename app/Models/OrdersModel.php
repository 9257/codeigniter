<?php
namespace App\Models;

use CodeIgniter\Model;

class OrdersModel extends Model
{
    protected $table      = 'orders';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['client_id', 'order_id', 'order_response', 'book_neighbour_response', 'book_neighbour_success', 'shipping_to_merchant_success', 'shipping_to_merchant_response', 'is_active', 'created', 'modified'];

    protected $useTimestamps = false;

    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>