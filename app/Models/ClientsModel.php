<?php
namespace App\Models;

use CodeIgniter\Model;

class ClientsModel extends Model
{
    protected $table      = 'clients';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['url', 'accesstoken', 'dropd_api_key', 'theme_id', 'theme_content'];

    protected $useTimestamps = false;

    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>