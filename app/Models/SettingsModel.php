<?php
namespace App\Models;

use CodeIgniter\Model;

class SettingsModel extends Model
{
    protected $table      = 'settings';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['client_id', 'customer_id', 'service_charge', 'is_active', 'product_id', 'variant_id', 'product_response', 'created', 'modified'];

    protected $useTimestamps = false;

    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>