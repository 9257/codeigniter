<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Libraries\Shopify; // Import library

class InstallAppController extends Controller {
    public function __construct() {
    }
    public function access(){
        $request = \Config\Services::request();
        $session = \Config\Services::session();
        $clients_model = new \App\Models\ClientsModel();
        $recurring_charge_model = new \App\Models\RecurringApplicationChargeModel();
        if($request->getVar('shop')) {
            $shop = $request->getVar('shop');
            $session->set('shop',$shop);
            $store_data = $clients_model->where('url', $shop)->first();
            if(!empty($store_data)){
                $user_session['access_token'] = $store_data['accesstoken'];
                $session->set($user_session);
                if(($session->has('access_token'))){
                    
                    $data = array(
                        'api_key' => getenv('API_KEY'),
                        'shop' => $shop
                    );
                    $data['id'] = $store_data['id'];

                    // get recurring:
                    $recur_data = $recurring_charge_model->where('client_id',$store_data['id'])->first();
                    if(!empty($recur_data)){
                        $data['is_recur_approve'] = $recur_data['is_approve'];
                        $data['recur_charge_id'] = $recur_data['id'];
                        $response = $recur_data['response'];
                        $confirmation_url = '';
                        if($response != ''){
                            $res = json_decode($response, true);
                            if(!empty($res)){
                                $confirmation_url = $res['recurring_application_charge']['confirmation_url'];
                                $data['confirmation_url'] = $confirmation_url;
                            }
                        }
                    }
                    return view('Admin/dashboard',$data);
                }
                else{
                    $this->auth($shop);
                }
            }
            else{
                $this->auth($shop);
            }
        } else {
            $errMsg = array('status' => false, 'message'=>'Shop url not empty');
            echo json_encode($errMsg);
        }
    }

    public function auth(){
        $request = \Config\Services::request();
        $shop = $request->getVar('shop');
        $data = array(
            'API_KEY' => getenv('API_KEY'),
            'API_SECRET' => getenv('API_SECRET'),
            'SHOP_DOMAIN' => $shop,
            'ACCESS_TOKEN' => ''
        );
        $shopify = new Shopify($data); // create an instance of Library
        $scopes = explode(',', getenv('SCOPES')); //what app can do
        $redirect_url = getenv('REDIRECT'); //redirect url specified in app setting at shopify
        $paramsforInstallURL = array(
            'scopes' => $scopes,
            'redirect' => $redirect_url
        );
        $permission_url = $shopify->installURL($paramsforInstallURL);
        // return redirect()->to('https://'.$shop.'/admin/apps');
        echo view('auth/escapeIframe',['installUrl' => $permission_url]);
    }

    public function authCallback(){
        $clients_model = new \App\Models\ClientsModel();
        $webhooks_model = new \App\Models\WebhooksModel();
        $request = \Config\Services::request();
        $session = \Config\Services::session();
        $code = $request->getVar('code');
        $shop = $request->getVar('shop');
        if(isset($code)){
            $data = array(
                'API_KEY' => getenv('API_KEY'),
                'API_SECRET' => getenv('API_SECRET'),
                'SHOP_DOMAIN' => $shop,
                'ACCESS_TOKEN' => ''
            );
            $shopify = new Shopify($data);
        }

        $accessToken = $shopify->getAccessToken($code);
        $data = array(
            'url' => $shop,
            'accesstoken' => $accessToken
        );
        
        $res = $clients_model->where('url', $shop)->first();
        if(!empty($res)) {
            $clients_model->where('url', $shop)->delete();
        }
        $clients_model->insert($data);
        $session->set(['access_token' => $accessToken]);
        $store_data = $clients_model->where('url', $shop)->first();
        if(!empty($store_data)){
            // create order webhook script
            $topic = 'orders/create';
            $address = base_url().'/create_orders';
            $create_order_webhook_response = $this->createWebhook($shop, $accessToken, $topic, $address);
            if(!empty($create_order_webhook_response)){
                $webhook_array_1 =  array(
                    'client_id' => $store_data['id'],
                    'webhook_id' => $create_order_webhook_response['webhook']['id'],
                    'topic' => $topic,
                    'address' => $address,
                    'response' => json_encode($create_order_webhook_response)
                );
                $webhooks_model->insert($webhook_array_1);
            }

            // create order fulfill webhook script
            $topic = 'orders/fulfilled';
            $address = base_url().'/fulfill_orders?shop_url='.$shop;
            $fulfill_order_webhook_response = $this->createWebhook($shop, $accessToken, $topic, $address);
            if(!empty($fulfill_order_webhook_response)){
                $webhook_array_3 =  array(
                    'client_id' => $store_data['id'],
                    'webhook_id' => $fulfill_order_webhook_response['webhook']['id'],
                    'topic' => $topic,
                    'address' => $address,
                    'response' => json_encode($fulfill_order_webhook_response)
                );
                $webhooks_model->insert($webhook_array_3);
            }
            // create delete app webhook script
            $delete_address = base_url().'/uninstall';
            $delete_topic = 'app/uninstalled';
            $delete_app_webhook_response = $this->createWebhook($shop, $accessToken, $delete_topic, $delete_address);
            if(!empty($delete_app_webhook_response)){
                $webhook_array_2 =  array(
                    'client_id' => $store_data['id'],
                    'webhook_id' => $delete_app_webhook_response['webhook']['id'],
                    'topic' => $delete_topic,
                    'address' => $delete_address,
                    'response' => json_encode($delete_app_webhook_response)
                );
                $webhooks_model->insert($webhook_array_2);
            }
            // create recurring application charge API:
            $recurring_api_responce = $this->RecurringApplicationCharge($shop,$accessToken);
            // $script_data = array(
            //     'script_tag' => array(
            //         'event' => getenv('SCRIPT_TAG_EVENT'),
            //         'src' => getenv('SCRIPT_ADDRESS')
            //     ),
            // );
            // $this->createScriptTag($shop,$accessToken,$script_data);

            // $script_data2 = array(
            //     'script_tag' => array(
            //         'event' => getenv('SCRIPT_TAG_EVENT'),
            //         'src' => getenv('SCRIPT_ADDRESS_2')
            //     ),
            // );
            // $this->createScriptTag($shop,$accessToken,$script_data2);

            // $script_data3 = array(
            //     'script_tag' => array(
            //         'event' => getenv('SCRIPT_TAG_EVENT'),
            //         'src' => getenv('SCRIPT_ADDRESS_3')
            //     ),
            // );
            // $this->createScriptTag($shop,$accessToken,$script_data3);

            // $script_data4 = array(
            //     'script_tag' => array(
            //         'event' => getenv('SCRIPT_TAG_EVENT'),
            //         'src' => getenv('SCRIPT_ADDRESS_4')
            //     ),
            // );
            // $this->createScriptTag($shop,$accessToken,$script_data4);
        }
        return redirect()->to('https://'.$shop.'/admin/apps');
    }
    public function approveRecurringCharge(){
        $request = \Config\Services::request();
        $clients_model = new \App\Models\ClientsModel();
        $recurring_model = new \App\Models\RecurringApplicationChargeModel();
        $charge_id = $request->getVar('charge_id');
        if($charge_id != ''){
            $recur_data = $recurring_model->where('recurring_application_charge_id', $charge_id)->first();
            if(!empty($recur_data)){
                $client_data = $clients_model->where('id', $recur_data['client_id'])->first();
                if(!empty($client_data)){
                    $up_data['is_approve'] = 1;
                    $up_data['modified'] = date('Y-m-d H:i:s');
                    $recurring_model->where('recurring_application_charge_id', $charge_id)->set($up_data)->update();

                    // get Theme and update theme files:
                    $theme_id = '';
                    $themes = $this->getThemes($client_data['url'],$client_data['accesstoken']);
                    if(!empty($themes)){
                        if(isset($themes['themes'])){
                            if(!empty($themes['themes'])){
                                foreach ($themes['themes'] as $themes_key => $theme) {
                                    if($theme['role'] == 'main'){
                                        $theme_id = $theme['id'];
                                    }
                                }
                            }
                        }
                    }
                    
                    $theme_content = '';
                    if($theme_id != ''){
                        $theme_data = $this->getThemesTamplateContents($theme_id,$client_data['url'],$client_data['accesstoken']);
                        // echo "<pre>";
                        // print_r($theme_data); die;
                        if(!empty($theme_data)){
                            if(isset($theme_data['asset'])){
                                if(!empty($theme_data['asset'])){
                                    if($theme_data['asset']['value'] != ''){
                                        $theme_content = $theme_data['asset']['value'];
                                    }
                                }
                            }
                        }
                    }

                    if($theme_content != ''){
                        //  Update Client
                        $client_up_data['theme_id'] = $theme_id;
                        $client_up_data['theme_content'] = $theme_content;
                        $clients_model->where('id', $client_data['id'])->set($client_up_data)->update();

                        $theme_data = '<script src="'.getenv('SCRIPT_ADDRESS').'"></script>';
                        $theme_data .= '<script src="'.getenv('SCRIPT_ADDRESS_2').'"></script>';
                        $theme_data .= '<script src="'.getenv('SCRIPT_ADDRESS_3').'"></script>';
                        // $theme_data .= '<script src="'.getenv('SCRIPT_ADDRESS_4').'"></script>';
                        $theme_data .= $theme_content;
                        $json_data_array = array(
                            "asset" => array(
                                "key" => "sections/cart-template.liquid",
                                "value" => $theme_data
                            )
                        );
                        $updated_data = $this->updateThemedata($theme_id,$client_data['url'],$client_data['accesstoken'],$json_data_array);
                        if(!empty($updated_data)){
                            return redirect()->to('https://'.$client_data['url'].'/admin/apps');
                        } else {
                            echo "Something went to wrong!";
                        }
                    }
                    
                } else {
                    echo "Something went to wrong!";
                }
            } else {
                echo "Something went to wrong!";
            }
            
        } else {
            echo "Something went to wrong!";
        }
    }
    public function createWebhook($shopUrl, $access_token, $topic, $address){
        $url = "https://".$shopUrl."/admin/api/2021-04/webhooks.json";
        $curl = curl_init();
        $json  = '{
          "webhook": {
            "topic": "'.$topic.'",
            "address": "'.$address.'",
            "format": "json"
          }
        }';
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => $json,
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'x-shopify-shop-api-call-limit: 1/40',
            'x-shopify-api-version: 2021-04',
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json'
          ),
        ));
        $response = curl_exec($curl);
        return json_decode($response, true);
    }
    public function RecurringApplicationCharge($shopUrl,$access_token) {
        $clients_model = new \App\Models\ClientsModel();
        $recurring_model = new \App\Models\RecurringApplicationChargeModel();
        $store_data = $clients_model->where('url', $shopUrl)->first();
        $client_id = '';
        if(!empty($store_data)){
            $client_id = $store_data['id'];
        }
        $data['recurring_application_charge'] = array(
            "name"=> getenv('RECURRING_APP_CHARGE_NAME'),
            "price"=> getenv('RECURRING_APP_CHARGE_PRICE'),
            "test"=>"true",
            "return_url"=> base_url().'/approve_charge',
            "capped_amount"=> getenv('RECURRING_APP_CAPPED_AMOUNT'),
            "terms"=> getenv('RECURRING_APP_TERMS')
        );

        $data = json_encode($data);
        $url = "https://".$shopUrl."/admin/api/2021-04/recurring_application_charges.json";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "cache-control: no-cache",
            "content-type: application/json",
            "Accept: application/json",
            "x-shopify-access-token: ".$access_token.""
        ));
        $response = curl_exec($ch);
        $response_array = json_decode($response,TRUE);
        if(isset($response_array['recurring_application_charge'])) {
            $recurring_application_charge = $response_array['recurring_application_charge'];
            $recurring_data = array(
                'recurring_application_charge_id' => $recurring_application_charge['id'],
                'client_id' => $client_id,
                'name' => $recurring_application_charge['name'],
                'price' => $recurring_application_charge['price'],
                'response' => json_encode($response_array)
            );
            $recurring_model->insert($recurring_data);
        } else {
            if($response_array['errors']) {
                foreach ($response_array['errors'] as $key => $value) {
                    if(is_array($value)) {
                        foreach ($value as $key1 => $value1) {
                            echo $key." - ".$value1;
                        }
                    } else {
                        echo $key." - ".$value;
                    }
                }
            }
        }
        return $response_array;
    }
    public function createScriptTag($shopUrl,$access_token,$data){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/script_tags.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($data, true),
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
    public function removeWebhook($shopUrl, $access_token, $webhook_id){
        $url = "https://".$shopUrl."/admin/api/2021-04/webhooks/".$webhook_id.".json";
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'DELETE',
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json'
          ),
        ));
        $response = curl_exec($curl);
        return json_decode($response, true);
    }
    public function removeProduct($product_id,$access_token, $shopUrl){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/products/".$product_id.".json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'DELETE',
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json',
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function uninstall(){
        $settings_model = new \App\Models\SettingsModel();
        $usage_charge_model = new \App\Models\UsageChargeModel();
        $recurring_model = new \App\Models\RecurringApplicationChargeModel();
        $clients_model = new \App\Models\ClientsModel();
        $webhooks_model = new \App\Models\WebhooksModel();
        $orders_model = new \App\Models\OrdersModel();
        $inputJSON = file_get_contents('php://input');
        $response= json_decode($inputJSON, TRUE);
        
        if(!empty($response))
        {
            $domain = $response['domain'];
            $store_data = $clients_model->where('url', $domain)->first();
            if(!empty($store_data)){
                // Get webhooks:
                $webhooks = $webhooks_model->where('client_id', $store_data['id'])->findAll();
                if(!empty($webhooks)){
                    foreach ($webhooks as $webhook_key => $webhook) {
                        $web_id = $webhook['id'];
                        $webhook_id = $webhook['webhook_id'];
                        // Remove webhooks From SHOP
                        $this->removeWebhook($store_data['url'], $store_data['accesstoken'], $webhook_id);
                        // Remove webhooks From DB
                        $webhooks_model->where('id', $web_id)->delete();
                    }
                }
                // Remove product:
                // Get settings
                $settings = $settings_model->where('client_id', $store_data['id'])->first();
                if(!empty($settings)){
                    $this->removeProduct($settings['product_id'], $store_data['accesstoken'], $store_data['url']);
                    // Remove webhooks From DB
                    $settings_model->where('id', $settings['id'])->delete();
                }
                // Usage Charge:
                $usage_charge_model->where('client_id', $store_data['id'])->delete();
                // Remove Recurring Charge:
                $recurring_model->where('client_id', $store_data['id'])->delete();
                // Remove Orders:
                $orders_model->where('client_id', $store_data['id'])->delete();
                // Remove Dropd code script js
                // $text_data = $store_data['theme_id']."\n";
                // $text_data .= $store_data['id']."\n";
                // $text_data .= $store_data['theme_content']."\n";
                if($store_data['theme_id'] != '' && $store_data['theme_content'] != ''){
                    $json_data_array = array(
                        "asset" => array(
                            "key" => "sections/cart-template.liquid",
                            "value" => $store_data['theme_content']
                        )
                    );
                    $updated_data = $this->updateThemedata($store_data['theme_id'],$store_data['url'],$store_data['accesstoken'],$json_data_array);
                    // $text_data .= json_encode($updated_data, true);
                }
                // Remove Store:
                $clients_model->where('id', $store_data['id'])->delete();
            }
            // $file =  getcwd().'/public/response.txt';
            // $myfile = fopen($file, "w") or die("Unable to open file!");
            // fwrite($myfile, $text_data);
        }
        return true;
    }
    public function getThemes($shopUrl,$access_token){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/themes.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function getThemesTamplateContents($theme_id,$shopUrl,$access_token){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/themes/".$theme_id."/assets.json?asset%5Bkey%5D=sections/cart-template.liquid",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function updateThemedata($theme_id,$shopUrl,$access_token,$data){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/themes/".$theme_id."/assets.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PUT',
          CURLOPT_POSTFIELDS => json_encode($data, true),
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
}