<?php
namespace App\Controllers;

use CodeIgniter\Controller;
// defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
header('P3P: CP="CAO PSA OUR"');
// header("Content-Type: application/json; charset=utf-8");

class Welcome extends Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function getRecurringApplicationCharge() {
		
		$shopUrl = 'dropd-development.myshopify.com';
		$SHOPIFY_ACCESS_TOKEN = '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';
		$url = "https://".$shopUrl."/admin/api/2021-04/recurring_application_charges.json";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"cache-control: no-cache",
	        "content-type: application/json",
	        "Accept: application/json",
	        "postman-token: b32be708-33f4-bbe0-42a5-99a284452bf8",
	        "x-shopify-access-token: ".$SHOPIFY_ACCESS_TOKEN.""
		));
		$response = curl_exec($ch);
		$res1 = json_decode($response,TRUE);
		if(!empty($res1['recurring_application_charges'])) {
			foreach ($res1['recurring_application_charges'] as $key => $value) {
				$data = array(
					'recurring_application_charge_id' => $value['id'],
					'name' => $value['name'],
					'price' => $value['price']
				);
				echo '<pre>';
				print_r($data);
			}
		}
	}

	public function fetchNeighboursByLatLong() {
		$request = \Config\Services::request();
		// $latitude = '37.8203';
		// $longitude = '144.8822';
		// $radiusInKM = '100000';
		// $result['neighbour_name'] = array();
		// $result['data'] = null;
		
		$result = array(
			'neighbour_name' => [],
			'data' => null
		);
		$postdata = $request->getPost();
		if(!empty($postdata)){

			

			// $latitude = $_POST['latitude'];
			// $longitude = $_POST['longitude'];
			$latitude = $request->getVar('latitude');
			$longitude = $request->getVar('longitude');
			$radiusInKM = '10';
			$url = 'https://aerial-ceremony-281103.ts.r.appspot.com/graphql/general';
			
			 $data='query
				{
					neighboursByLatLong(latitude: '.$latitude.', longitude: '.$longitude.', radiusInKM:'.$radiusInKM.') {
						id
						name
						rating
						ratingCount
						availability
						latitude
						longitude
						photoURL
						distanceInKM
						country
						state
						city
						postCode
					}
			}';
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				"cache-control: no-cache",
		        "content-type: application/graphql",
		        "Accept: application/graphql",
		        "postman-token: c66c80f2-0e14-4247-9ad1-c1f3a3f45900"
			));
			$response = curl_exec($ch);
			$res1 = json_decode($response,TRUE);
			$nameArr = array();

			if(isset($res1['data']['neighboursByLatLong'])) {
				$res = $res1['data']['neighboursByLatLong'];
				$finalArr[] = $res;
				foreach ($res as $key => $value) {
					$name = $value['name'];
					$nameArr[] = $name;
				}
			}

			$result = array(
				'neighbour_name' => $nameArr,
				'data' => $finalArr
			);
		}
		print json_encode($result,true);
		die;
	}	
}
