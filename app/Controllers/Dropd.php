<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Dropd extends Controller {
    public $request;
    public $session;
    public $clients_model;
    public $settings_model;
    public $recurring_model;
    public $usage_charge_model;
    public function __construct() {
        $this->request = \Config\Services::request();
        $this->session = \Config\Services::session();
        $this->clients_model = new \App\Models\ClientsModel();
        $this->settings_model = new \App\Models\SettingsModel();
        $this->orders_model = new \App\Models\OrdersModel();
        $this->recurring_model = new \App\Models\RecurringApplicationChargeModel();
        $this->usage_charge_model = new \App\Models\UsageChargeModel();
    }
    public function index($id){
        if($id != ''){
            $data['id'] = base64_decode($id);
            return view('Admin/dashboard',$data);
        }
    }
    public function settings($id){
        if($id != ''){
            $data['id'] = base64_decode($id);
            $settings = $this->settings_model->where('client_id', $data['id'])->first();
            $data['settings'] = $settings;
            return view('Admin/settings',$data);
        }
    }
    public function getVariantID(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
        header('P3P: CP="CAO PSA OUR"');
        $product_id = $this->request->getVar('product_id'); 
        $shop_url = $this->request->getVar('shop_url');
        $response = array();
        $response['success'] = 'false';
        if($shop_url != '' && $product_id != ''){
            $clients = $this->clients_model->where('url', $shop_url)->first();
            if(!empty($clients)){
                $variants = $this->getVariantByProductId($product_id, $clients['accesstoken'],$shop_url);
                $variant_id = '';
                if(!empty($variants)){
                   $variant_id = $variants['variants'][0]['id'];
                   $response['success'] = 'true';
                   $response['variant_id'] = $variant_id;
                    
                } else {
                    $response['message'] = 'variant does not exist!';
                }
            } else {
                $response['message'] = 'variant does not exist!';
            }
        } else {
            $response['message'] = 'variant does not exist!';
        }
        print json_encode($response, true);
        die;
    }
    public function getProductID(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
        header('P3P: CP="CAO PSA OUR"');
        $shop_url = $this->request->getVar('shop_url');
        $response = array();
        $response['success'] = 'false';
        if($shop_url != ''){
            $clients = $this->clients_model->where('url', $shop_url)->first();
            if(!empty($clients)){
                $settings = $this->settings_model->where('client_id', $clients['id'])->first();
                if(!empty($settings)){
                    $response['success'] = 'true';
                    $response['product_id'] = $settings['product_id'];
                    $response['variant_id'] = $settings['variant_id'];
                } else {
                    $response['message'] = 'Product does not exist!';
                }
            } else {
                $response['message'] = 'Product does not exist!';
            }
        } else {
            $response['message'] = 'Product does not exist!';
        }
        print json_encode($response, true);
        die;
    }
    public function orderStatus(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
        header('P3P: CP="CAO PSA OUR"');
        $data['data'] = $_REQUEST;
        return view('Admin/go_back',$data);
        die;
    }
    public function settingsSubmit(){
        $data = $this->request->getPost();
        if(!empty($data)){
            $product_data = array(
                'product' => array(
                    'title' => getenv('DROPD_CHARGE_PRODUCT_TITLE'),
                    'handle' => getenv('DROPD_CHARGE_PRODUCT_HANDLE'),
                    'variants' => array(array(
                        "price" =>  $data['service_charge'],
                        "sku" => getenv('DROPD_CHARGE_PRODUCT_SKU'),
                        "inventory_management" => null,
                    )),
                    'images' => array(array(
                        "src" => base_url().'/public/admin/images/DropdLogo-Icon.png',
                    ))
                )
            );
            $clients = $this->clients_model->where('id', $data['client_id'])->first();
            if(!empty($clients)){
                if($data['settings_id'] != ''){
                    $update_data = array(
                        'customer_id' => $data['customer_id'],
                        'service_charge' => $data['service_charge'],
                        'modified' => date('Y-m-d H:i:s')
                    );
                    $this->settings_model->where('id', $data['settings_id'])->set($update_data)->update();
                    // update product:
                   $product_data = $this->updateProduct($product_data,$data['product_id'], $clients['accesstoken'],$clients['url']);
                    if(!empty($product_data)){
                        if(isset($product_data['product'])){
                            if(!empty($product_data['product'])){
                                $response = json_encode($product_data, true);
                                $up_data['product_response'] =$response;
                                $up_data['variant_id'] =$product_data['product']['variants'][0]['id'];
                                $up_data['modified'] = date('Y-m-d H:i:s');
                                $this->settings_model->where('id', $data['settings_id'])->set($up_data)->update();
                            }
                        }
                    }
                    
                } else {
                    unset($data['settings_id']);
                    $this->settings_model->insert($data);
                    $setting_id  = $this->settings_model->getInsertID();
                    // create test product
                    $product_data = $this->createProduct($product_data, $clients['accesstoken'],$clients['url']);
                    $variant_id = $product_id ='';
                    if(!empty($product_data)){
                        if(isset($product_data['product'])){
                            if(!empty($product_data['product'])){
                                $product_id = $product_data['product']['id'];
                                if(isset($product_data['product']['variants'][0])){
                                    if(!empty($product_data['product']['variants'][0])){
                                        $variant_id = $product_data['product']['variants'][0]['id'];
                                        $response = json_encode($product_data, true);
                                        $up_data['product_id'] =$product_id;
                                        $up_data['variant_id'] =$variant_id;
                                        $up_data['product_response'] =$response;
                                        $up_data['modified'] = date('Y-m-d H:i:s');
                                        $this->settings_model->where('id', $setting_id)->set($up_data)->update();
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            $data['message'] = "Sorry, you must login first";
            $this->session->setFlashdata('success', 'Your settings is successfully saved.');
            return redirect()->to(base_url().'/settings/'.base64_encode($data['client_id']));
        }
    }
    public function getVariantByProductId($product_id,$access_token, $shopUrl){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/products/".$product_id."/variants.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json',
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function createProduct($data,$access_token, $shopUrl){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/products.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($data, true),
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json',
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function updateProduct($data,$product_id,$access_token, $shopUrl){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://".$shopUrl."/admin/api/2021-04/products/".$product_id.".json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PUT',
          CURLOPT_POSTFIELDS => json_encode($data, true),
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json',
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function getShopifyOrders($params, $access_token, $shopUrl)
    {
        if($params["page_info"] != null)
        {
            $url = "https://".$shopUrl."/admin/orders.json?limit=250&page_info=" . $params["page_info"];
        }
        else
        {
            $url = "https://".$shopUrl."/admin/orders.json?".http_build_query($params);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "cache-control: no-cache",
            "content-type: application/json",
            "Accept: application/json",
            "x-shopify-access-token: ".$access_token.""
        ));
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($ch, $header) use (&$headers) {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) >= 2) {
                $headers[strtolower(trim($header[0]))] = trim($header[1]);
            }
            return $len;
        });
        $result = curl_exec($ch);
        curl_close($ch);
        $tokenType = 'next';
        if(isset($headers['link'])) {
            $links = explode(',', $headers['link']);
            foreach($links as $link) {
                if(strpos($link, 'rel="next"')) {
                    $tokenType  = strpos($link,'rel="next') !== false ? "next" : "previous";
                    preg_match('~<(.*?)>~', $link, $next);
                    $url_components = parse_url($next[1]);
                    parse_str($url_components['query'], $paramss);
                    $pageToken = trim($paramss['page_info']);
                }
            }
        }
        $source_array = json_decode($result, true);
        $r['orders'] =  (is_array($source_array) && count($source_array) > 0) ? array_shift($source_array) : $source_array;
        $r[$tokenType]['page_token'] = isset($pageToken) ? $pageToken : null;
        return $r;
    }

    public function createOrder(){
        $inputJSON = file_get_contents('php://input');

        $file =  getcwd().'/public/response.txt';
        $myfile = fopen($file, "w") or die("Unable to open file!");
        fwrite($myfile, $inputJSON);

        $o_value= json_decode($inputJSON, TRUE);

        if(!empty($o_value))
        {
            $note_attributes = $o_value['note_attributes'];
            $shop_url = $neighbourId = $cname = $address1 = $city = $country = $province = $zip = $customer_address = $customer_phone =  null;
            if(!empty($note_attributes)){
                foreach ($note_attributes as $attr_key => $attributes) {
                    if($attributes['name'] == 'DropID'){
                        $neighbourId = $attributes['value']; 
                    }
                    if($attributes['name'] == 'shopUrl'){
                        $shop_url = $attributes['value']; 
                    }
                    if($attributes['name'] == 'address1'){
                        $address1 = $attributes['value']; 
                    }
                    if($attributes['name'] == 'name'){
                        $cname = $attributes['value']; 
                    }
                    if($attributes['name'] == 'city'){
                        $city = $attributes['value']; 
                    }
                    if($attributes['name'] == 'country'){
                        $country = $attributes['value']; 
                    }
                    if($attributes['name'] == 'province'){
                        $province = $attributes['value']; 
                    }
                    if($attributes['name'] == 'zip'){
                        $zip = $attributes['value']; 
                    }
                }
            }
            if($neighbourId != ''){
                $client_data = $this->clients_model->where('url', $shop_url)->first();
                if(!empty($client_data)){
                    $ordrer_exist_data = $this->orders_model->where('client_id', $client_data['id'])->where('order_id', $o_value['id'])->first();
                    if(empty($ordrer_exist_data)){
                        // get customer_id
                        $customer_id  = $chargeId = '';
                        $settings_data = $this->settings_model->where('client_id', $client_data['id'])->first();
                        if(!empty($settings_data)){
                            $customer_id = $settings_data['customer_id'];
                        }
                        $recurring_data = $this->recurring_model->where('client_id', $client_data['id'])->first();
                        if(!empty($recurring_data)){
                            $chargeId = $recurring_data['recurring_application_charge_id'];
                        }

                        $order_array = array(
                            'order_id' => $o_value['id'],
                            'client_id' => $client_data['id'],
                            'order_response' => json_encode($o_value),
                        );

                        $this->orders_model->insert($order_array);
                        $oid  = $this->orders_model->getInsertID();

                        $customer = $o_value['customer'];
                        if(!empty($customer)){
                            $customer_phone = $customer['phone'];
                            if($customer_phone == ''){
                                $customer_phone = null;
                            }
                        }
                        $customer_address = $address1.', '.$city.', '.$province.', '.$country.', '.$zip;
                        $data_api = array(
                            'neighbourId' => $neighbourId,
                            'merchantOrderId' => $o_value['id'],
                            'customerName' => $cname,
                            'customerEmail' => $o_value['email'],
                            'customerTelephone' => $customer_phone,
                            'customerAddress' => $customer_address,
                        );
                        $bookNeighbour_data = $this->bookNeighboar($data_api, $customer_id, $client_data['dropd_api_key']);
                        if(!isset($bookNeighbour_data['errors'])){
                            if(!empty($bookNeighbour_data)){
                                if(isset($bookNeighbour_data['data']['bookNeighbour'])){
                                    if(!empty($bookNeighbour_data['data']['bookNeighbour'])){
                                        $is_success = $bookNeighbour_data['data']['bookNeighbour']['isSuccess'];
                                        $up_book_data['book_neighbour_response'] = json_encode($bookNeighbour_data, true);
                                        $up_book_data['book_neighbour_success'] = $is_success;
                                        $up_book_data['modified'] = date('Y-m-d H:i:s');

                                        // Update shipping address:
                                        $neighboar_address = $bookNeighbour_data['data']['bookNeighbour']['neighbour'];
                                        if(!empty($neighboar_address)){
                                            $order_up_data = array(
                                              "order" => array(
                                                "id" => $o_value['id'],
                                                "shipping_address" => array(
                                                  "address1" => $neighboar_address['address'],
                                                  "city" => $neighboar_address['city'],
                                                  "phone" => $neighboar_address['phoneNumber'],
                                                  "province" => $neighboar_address['state'],
                                                  "country" => $neighboar_address['country'],
                                                  "zip" => $neighboar_address['postcode'],
                                                ),
                                              ),
                                            );
                                            $update_order_data = $this->updteShippingAddress($o_value['id'], $order_up_data, $shop_url, $client_data['accesstoken']);
                                        }
                                    } else {
                                        $up_book_data['book_neighbour_response'] = json_encode($bookNeighbour_data, true);
                                        $up_book_data['modified'] = date('Y-m-d H:i:s');
                                    }
                                }
                            } else {
                                $up_book_data['book_neighbour_response'] = json_encode($bookNeighbour_data, true);
                                $up_book_data['modified'] = date('Y-m-d H:i:s');
                            }
                        } else {
                            $up_book_data['book_neighbour_response'] = json_encode($bookNeighbour_data, true);
                            $up_book_data['modified'] = date('Y-m-d H:i:s');
                        }
                        $this->orders_model->where('id', $oid)->set($up_book_data)->update();

                        // recurring api:
                        $description = $o_value['id'].' - order usage charge applied';
                        $this->addUsageCharge($client_data['id'],$client_data['url'],$client_data['accesstoken'],$chargeId,$description, getenv('USAGE_SERVICE_CHARGE'));
                    }
                }
            }
        }
        return true;
    }
    public function  updteShippingAddress($order_id, $data, $shopUrl, $access_token){
        $url = "https://".$shopUrl."/admin/api/2021-04/orders/".$order_id.".json";
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PUT',
          CURLOPT_POSTFIELDS => json_encode($data, TRUE),
          CURLOPT_HTTPHEADER => array(
            'x-shopify-access-token: '.$access_token,
            'content-type: application/json',
            'cache-control: no-cache',
            'Accept: application/json',
            'Cookie: _secure_admin_session_id=f71ec3e167b5fc98aa68ec7d7f3740a8; _secure_admin_session_id_csrf=f71ec3e167b5fc98aa68ec7d7f3740a8; _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWsyTXpJMFpURmhPUzB4TWpobExUUTFPVEF0T0ROaU55MDFNMk13TTJaa01UUTVNak1HT2daRlJnPT0iLCJleHAiOiIyMDIzLTA1LTIyVDEwOjMzOjE0LjI1NVoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--8fab46e856c580f5f7af34ac87e240fe2e84e4da; _y=aa665304-9606-4d87-9da9-98ce5885a0b6; _shopify_y=aa665304-9606-4d87-9da9-98ce5885a0b6'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function bookNeighboar($data, $customer_id, $dropd_api_key){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => getenv('BOOK_NEIGHBOUR_URL'),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"query":"mutation BookNeighbour(\\r\\n    $neighbourId:ID!,\\r\\n    $merchantOrderId:ID!,\\r\\n    $customerName:String!,\\r\\n    $customerEmail:String!,\\r\\n    $customerTelephone:String,\\r\\n    $customerAddress:String!) {\\r\\n    bookNeighbour(\\r\\n        neighbourId: $neighbourId,\\r\\n        merchantOrderID: $merchantOrderId,\\r\\n        customer: {\\r\\n            name: $customerName,\\r\\n            email: $customerEmail,\\r\\n            telephone: $customerTelephone,\\r\\n            address: $customerAddress\\r\\n        }) {\\r\\n        isSuccess,\\r\\n        errorId,\\r\\n        errorMessage,\\r\\n        booking {\\r\\n            merchantOrderId,\\r\\n            customer {\\r\\n                name,\\r\\n                telephone,\\r\\n                address\\r\\n            }\\r\\n        },\\r\\n        neighbour {\\r\\n            firstName,\\r\\n            lastName,\\r\\n            email,\\r\\n            phoneNumber,\\r\\n            address,\\r\\n            city,\\r\\n            state,\\r\\n            postcode,\\r\\n            country\\r\\n        }\\r\\n    }\\r\\n}","variables": '.json_encode($data, true).'}',
          CURLOPT_HTTPHEADER => array(
            'X-Dropd-CustomerID: '.$customer_id,
            'x-api-key: '.$dropd_api_key,
            "cache-control: no-cache",
            "content-type: application/json",
            "Accept: application/json",
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function fulfillOrder(){
        $inputJSON = file_get_contents('php://input');
        // $inputJSON = file_get_contents(getcwd().'/public/test.json');
        // $file =  getcwd().'/public/response.txt';
        // $myfile = fopen($file, "w") or die("Unable to open file!");
        $o_value= json_decode($inputJSON, TRUE);
        // fwrite($myfile, $inputJSON);
        
        if(!empty($o_value))
        {
            $note_attributes = $o_value['note_attributes'];
            $shop_url = $neighbourId = '';
            if(!empty($note_attributes)){
                foreach ($note_attributes as $attr_key => $attributes) {
                    if($attributes['name'] == 'shopUrl'){
                        $shop_url = $attributes['value']; 
                    }
                }
            }
            $client_data = $this->clients_model->where('url', $shop_url)->first();
            if(!empty($client_data)){
                // get customer_id
                $customer_id = $service_charge = '';
                $settings_data = $this->settings_model->where('client_id', $client_data['id'])->first();
                if(!empty($settings_data)){
                    $customer_id = $settings_data['customer_id'];
                    $service_charge = $settings_data['service_charge'];
                }
                
                if(!empty($o_value['fulfillments'])){
                    $data_api = array(
                    'orderId' => $o_value['id'],
                    'courierSlug' => 'ups',
                    'trackingNumber' => $o_value['fulfillments'][0]['tracking_number'],
                    );

                    $addShippingPackage_data = $this->addShippingPackageToMerchantOrder($data_api, $customer_id, $client_data['dropd_api_key']);
                    
                    if(!empty($addShippingPackage_data)){
                        if(isset($addShippingPackage_data['data']['addShippingPackageToMerchantOrder']['isSuccess'])){
                            $is_success = $addShippingPackage_data['data']['addShippingPackageToMerchantOrder']['isSuccess'];
                            if($is_success != ''){
                                $is_success = $addShippingPackage_data['data']['addShippingPackageToMerchantOrder']['isSuccess'];
                            } else{
                                $is_success = 0;
                            } 
                            $up_data['shipping_to_merchant_success'] = $is_success;
                            $up_data['shipping_to_merchant_response'] = json_encode($addShippingPackage_data);
                            $this->orders_model->where('order_id', $o_value['id'])->where('client_id', $client_data['id'])->set($up_data)->update();
                        }
                    }
                }
            }
        }
        return true;
    }
    public function addShippingPackageToMerchantOrder($data, $customer_id, $dropd_api_key){
        $curl = curl_init();
          curl_setopt_array($curl, array(
          CURLOPT_URL => getenv('ADD_SHIP_TO_MERCHANT_URL'),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"query":"mutation AddShippingPackageToMerchantOrder($orderId:ID!, $courierSlug:String, $trackingNumber:String!){\\r\\n    addShippingPackageToMerchantOrder(\\r\\n        orderId: $orderId,\\r\\n        courierSlug: $courierSlug,\\r\\n        trackingNumber: $trackingNumber\\r\\n    ) {\\r\\n        isSuccess,\\r\\n        errorId,\\r\\n        errorMessage\\r\\n    }\\r\\n}","variables":'.json_encode($data, true).'}',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'X-Dropd-CustomerID: '.$customer_id,
            'x-api-key: '.$dropd_api_key
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    public function getCharges($client_id){
        $data['clients'] = $this->clients_model->where('id', base64_decode($client_id))->first();
        $data['all_charges'] = $this->usage_charge_model->where('is_active', 1)->where('client_id', base64_decode($client_id))->orderBy('created', 'DESC')->findAll();
        $data['id'] = base64_decode($client_id);
        return view('Admin/charges',$data);
    }
    public function addUsageCharge($client_id,$shopUrl,$access_token,$chargeId,$description,$price) {
        $data['usage_charge'] = array(
            "description"=>$description,
            "price"=> (float) $price
        );
        $data = json_encode($data);
        $url = "https://".$shopUrl."/admin/api/2021-04/recurring_application_charges/".$chargeId."/usage_charges.json";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "cache-control: no-cache",
            "content-type: application/json",
            "Accept: application/json",
            "x-shopify-access-token: ".$access_token.""
        ));
        $response = curl_exec($ch);
        $res1 = json_decode($response,TRUE);
       
        if(isset($res1['usage_charge'])) {
            $val = $res1['usage_charge'];
            $created = $val['created_at'];
            $c_date = '';
            if($created != ''){
                $c_date = str_replace('T', ' ', $created);
            }
            $data = array(
                'client_id' => $client_id,
                'usage_charge_id' => $val['id'],
                'description' => $val['description'],
                'price' => $val['price'],
                'created_at' => $c_date,
                'billing_on' => $val['billing_on'],
                'balance_used' => $val['balance_used'],
                'balance_remaining' => $val['balance_remaining'],
                'response' => $response,
                'risk_level' => $val['risk_level']
            );
            $this->usage_charge_model->insert($data);
            
        } else {
            echo "Something went to wrong";
        }
    }
    public function getOrders($id){
        $data['id'] = base64_decode($id);
        $store_data = $this->clients_model->where('id', base64_decode($id))->first();
        if(!empty($store_data)){
            $orders_data = $this->orders_model->where('client_id', base64_decode($id))->where('is_active', 1)->orderBy('created', 'DESC')->findAll();
            $data['orders'] = $orders_data;
            $data['clients'] = $this->clients_model->where('id', base64_decode($id))->first();
            return view('Admin/orders',$data);
        }
    }
    public function fetchNeighboursByLatLong() {
        // $latitude = '37.8203';
        // $longitude = '144.8822';
        // $radiusInKM = '100000';
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
        header('P3P: CP="CAO PSA OUR"');
        header("Content-Type: application/json; charset=utf-8");
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $radiusInKM = '100';
        $url = 'https://aerial-ceremony-281103.ts.r.appspot.com/graphql/general';
        $data='query 
                {
                    neighboursByLatLong(latitude: '.$latitude.', longitude: '.$longitude.', radiusInKM:'.$radiusInKM.') {
                        id
                        name
                        rating
                        ratingCount
                        availability
                        latitude
                        longitude
                        photoURL
                        distanceInKM
                    }
            }';
                
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "cache-control: no-cache",
            "content-type: application/graphql",
            "Accept: application/graphql",
            "postman-token: b32be708-33f4-bbe0-42a5-99a284452bf8"
        ));
        $response = curl_exec($ch);
        $res1 = json_decode($response,TRUE);
        $nameArr = array();

        if(isset($res1['data']['neighboursByLatLong'])) {
            $res = $res1['data']['neighboursByLatLong'];
            $finalArr[] = $res;
            foreach ($res as $key => $value) {
                $name = $value['name'];
                $nameArr[] = $name;
            }
        }

        $finalArr1 = array(
            'neighbour_name' => $nameArr,
            'data' => $finalArr
        );

        echo json_encode($finalArr1);
    }   
    
}
