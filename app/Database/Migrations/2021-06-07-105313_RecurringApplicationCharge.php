<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RecurringApplicationCharge extends Migration
{
	public function up()
	{
		$this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'unsigned'       => true,
                        'auto_increment' => true,
                ],
                'client_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'null' => true,
                       
                ],
                'recurring_application_charge_id'          => [
                        'type'           => 'BIGINT',
                        'constraint'     => 20,
                        'null' => true,
                       
                ],
                'name'       => [
                        'type'       => 'VARCHAR',
                        'constraint'     => 50,
                        'null' => true,
                ],
                'price'          => [
                        'type'           => 'DECIMAL',
                        'constraint' => '10,2',
                        'null' => true,
                       
                ],
                'response'       => [
                        'type'       => 'TEXT',
                        'null' => true,
                ],
                'is_approve' => [
                        'type' => 'TINYINT',
                        'default' => 0

                ],
                'is_active' => [
                        'type' => 'TINYINT',
                        'default' => 1

                ],
                'created datetime default current_timestamp',
                'modified datetime default current_timestamp on update current_timestamp', 
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('recurring_application_charge');
	}

	public function down()
	{
		$this->forge->dropTable('recurring_application_charge');
	}
}
