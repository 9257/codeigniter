<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Webhooks extends Migration
{
	public function up()
	{
		$this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'unsigned'       => true,
                        'auto_increment' => true,
                ],
                'client_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'null' => true,
                       
                ],
                'webhook_id'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                        'null' => true,
                       
                ],
                'topic'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                        'null' => true,
                       
                ],
                'address'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                        'null' => true,
                       
                ],
                'response'       => [
                        'type'       => 'TEXT',
                        'null' => true,
                ],
                'is_active' => [
                        'type' => 'TINYINT',
                        'default' => 1

                ],
                'created datetime default current_timestamp',
                'modified datetime default current_timestamp on update current_timestamp', 
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('webhooks');
	}

	public function down()
	{
		$this->forge->dropTable('webhooks');
	}
}
