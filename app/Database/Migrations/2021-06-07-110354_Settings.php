<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Settings extends Migration
{
	public function up()
	{
		$this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'unsigned'       => true,
                        'auto_increment' => true,
                ],
                'client_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'null' => true,
                       
                ],
                'customer_id'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                        'null' => true,
                       
                ],
                'service_charge'          => [
                        'type'           => 'DECIMAL',
                        'constraint' => '10,2',
                        'null' => true,
                       
                ],
                'is_active' => [
                        'type' => 'TINYINT',
                        'default' => 1

                ],
                'product_id' => [
                        'type' => 'BIGINT',
                        'null' => true,

                ],
                'variant_id' => [
                        'type' => 'BIGINT',
                        'null' => true,

                ],
                'product_response'       => [
                        'type'       => 'TEXT',
                        'null' => true,
                ],
                'created datetime default current_timestamp',
                'modified datetime default current_timestamp on update current_timestamp', 
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('settings');
	}

	public function down()
	{
		$this->forge->dropTable('settings');
	}
}
