<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Orders extends Migration
{
	public function up()
	{
		$this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'unsigned'       => true,
                        'auto_increment' => true,
                ],
                'client_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'null' => true,
                       
                ],
                'order_id'          => [
                        'type'           => 'BIGINT',
                        'constraint'     => 20,
                       
                ],
                'order_response'       => [
                        'type'       => 'TEXT',
                        'null' => true,
                ],
                'book_neighbour_response'       => [
                        'type'       => 'TEXT',
                        'null' => true,
                ],
                'book_neighbour_success'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'null' => true,
                        'default' => 0
                       
                ],
                'shipping_to_merchant_success'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'null' => true,
                        'default' => 0
                       
                ],
                'shipping_to_merchant_response'       => [
                        'type'       => 'TEXT',
                        'null' => true,
                ],
                'is_active' => [
                        'type' => 'TINYINT',
                        'default' => 1

                ],
                'created datetime default current_timestamp',
                'modified datetime default current_timestamp on update current_timestamp', 
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('orders');
	}

	public function down()
	{
		$this->forge->dropTable('orders');
	}
}
