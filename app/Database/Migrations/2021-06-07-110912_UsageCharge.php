<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UsageCharge extends Migration
{
	public function up()
	{
		$this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'unsigned'       => true,
                        'auto_increment' => true,
                ],
                'client_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'null' => true,
                       
                ],
                'usage_charge_id'          => [
                        'type'           => 'BIGINT',
                        'constraint'     => 20,
                        'null' => true,
                       
                ],
                'description'          => [
                        'type'           => 'TEXT',
                        'null' => true,
                       
                ],
                'price'          => [
                        'type'           => 'DECIMAL',
                        'constraint' => '10,2',
                        'null' => true,
                       
                ],
                'created_at'          => [
                        'type'           => 'DATETIME',
                        'null' => true,
                       
                ],
                'billing_on'          => [
                        'type'           => 'DATE',
                        'null' => true,
                       
                ],
                'balance_used'          => [
                        'type'           => 'DECIMAL',
                        'constraint' => '10,2',
                        'null' => true,
                       
                ],
                'balance_remaining'          => [
                        'type'           => 'DECIMAL',
                        'constraint' => '10,2',
                        'null' => true,
                       
                ],
                'response'       => [
                        'type'       => 'TEXT',
                        'null' => true,
                ],
                'risk_level'       => [
                        'type'       => 'INT',
                        'null' => true,
                ],
                'is_active' => [
                        'type' => 'TINYINT',
                        'default' => 1

                ],
                'created datetime default current_timestamp',
                'modified datetime default current_timestamp on update current_timestamp', 
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('usage_charge');
	}

	public function down()
	{
		$this->forge->dropTable('usage_charge');
	}
}
