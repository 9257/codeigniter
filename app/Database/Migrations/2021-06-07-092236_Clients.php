<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Clients extends Migration
{
	public function up()
	{
		$this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 11,
                        'unsigned'       => true,
                        'auto_increment' => true,
                ],
                'url'       => [
                        'type'       => 'TEXT'
                ],
                'accesstoken' => [
                        'type' => 'TEXT'
                ],
                'dropd_api_key'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                        'null' => true,
                       
                ],
                'theme_id'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                        'null' => true,
                       
                ],
                'theme_content' => [
                        'type' => 'TEXT',
                        'null' => true,
                ],
                'created datetime default current_timestamp',
                'modified datetime default current_timestamp on update current_timestamp', 
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('clients');
	}

	public function down()
	{
		$this->forge->dropTable('clients');
	}
}
