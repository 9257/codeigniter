var lat;
var lng;
var locations = [];
var htmladd;
$(document).ready(function() {
  	if ($(".cart__buttons-container").length > 0) {
	    var map_popup = '<div class="collect-btn">';         
      map_popup +='<a href="#"><img src="https://dropd.dynamicdreamz.com/DropDev/public/admin/images/DropdLogo-Icon.png">Click & Collect with dropd</a>';
      map_popup +='</div>';
	    map_popup += '<div id="search_container" class="search_container" style="display:none;">';
	    map_popup += '<div class="text-right">';
	    map_popup += '<img src="https://dropd.dynamicdreamz.com/DropDev/public/admin/images/dropdlogo-100.svg" style="vertical-align: middle;margin-right:10px;height: 46px;width: 100px;">';
	    map_popup += '<div class="search-search">';
		  map_popup += '<i class="far fa-dot-circle icon"></i>';
	    map_popup += '<input id="pac-input" class="controls" type="text" placeholder="Search for your nearest dropd neighbour" style="margin-bottom: 10px;">';	   
	    map_popup += '<div class="listView" style="display: inline-block;margin-left: 0;position: relative;">';
	    map_popup += '<i class="fas fa-search icon"></i>';
	    map_popup += '<ul class="top-btn-list" style="display:none;">';
	    map_popup += '<li><button type="button" onclick="getMapView(event);"><i class="fa fa-map-marker-alt" ></i></button></li>';
	    map_popup += '<li><button type="button" onclick="getListView(event);"><i class="fa fa-list" ></i></button></li>';
	    map_popup += '</ul>';
	    map_popup += '</div>';
	    map_popup += '</div>';
	    map_popup += '</div>';
	    map_popup += '<div id="map" style="display: none;"></div>';
	    map_popup += '<div id="list" style="display: none;"></div>';
	    map_popup += '</div>';
	    map_popup += '<div id="search-popup" class="white-popup mfp-hide">';
	    map_popup += '<h3 id="search_res_title"></h3>';
	    map_popup += '<div id="search_res_body"></div>';
	    map_popup += '</div>';
	    map_popup += '<a href="#search-popup" class="btn open-popup-link search_popup_link" style="display:none;">search</a>';
	    $(map_popup).insertBefore('.cart__buttons-container');
      var map_js = document.createElement("script");
      map_js.type = "text/javascript";
      map_js.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyC6etb4KPprWQK-v_JCUEH7M2N2eGj-t24&callback=initAutocomplete&libraries=places&v=weekly";
      $("head").append(map_js);
	  }
  	$('.search_popup_link').magnificPopup({
  	    type: 'inline',
  	    midClick: true,
  	    mainClass: 'mfp-fade'
  	});	
  	$("<link/>", {
  	   rel: "stylesheet",
  	   type: "text/css",
  	   href: "https://dropd.dynamicdreamz.com/DropDev/public/magnific-popup.css"
  	}).appendTo("head");

  	$("<link/>", {
  	   rel: "stylesheet",
  	   type: "text/css",
  	   href: "https://dropd.dynamicdreamz.com/DropDev/public/custom-magnific.css"
  	}).appendTo("head");

  	$("<link/>", {
  	   rel: "stylesheet",
  	   type: "text/css",
  	   href: "https://dropd.dynamicdreamz.com/DropDev/public/custom.css"
  	}).appendTo("head");

  	$("<link/>", {
  	   rel: "stylesheet",
  	   type: "text/css",
  	   href: "https://dropd.dynamicdreamz.com/DropDev/public/fontawesome-all.css?v=123"
  	}).appendTo("head");
});
$(document).on("click",'.collect-btn',function(){
    $('.search_container').css('display','block')
});
// $( function() {
//   $('#pac-input-1').bind('keyup', function(){
//     getListViewData();
//     $('#pac-input-1').unbind('keyup');  //or $(this).unbind probably a tad faster
//   });
// });
function initAutocomplete() {
    const map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 10,
      mapTypeId: "roadmap",
      disableDefaultUI: true,
    });
    // Create the search box and link it to the UI element.
    const input = document.getElementById("pac-input");
    const searchBox = new google.maps.places.SearchBox(input);
	  // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", () => {
      searchBox.setBounds(map.getBounds());
  	});
  	let markers = [];
  	// Listen for the event fired when the user selects a prediction and retrieve
  	// more details for that place.
  	searchBox.addListener("places_changed", () => {
       	const places = searchBox.getPlaces();
      	if (places.length == 0) {
          return;
        } // Clear out the old markers.
      	markers.forEach((marker) => {
            marker.setMap(null);
        });
      	//markers = []; 
      	// For each place, get the icon, name and location.
  		  const bounds = new google.maps.LatLngBounds();
        places.forEach((place) => {
          if (!place.geometry || !place.geometry.location) {
            return;
          }
          const icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25),
          };
          // Create a marker for each place.
          lat = place.geometry.location.lat();
          lng = place.geometry.location.lng();
          getListViewData(lat,lng);//   
          $.ajax({
            method : "POST",
            url : "https://dropd.dynamicdreamz.com/DropDev/fetchNeighboursByLatLong",
            data : {"latitude":lat,"longitude":lng,"radiusInKM":radiusInKM},
            dataType : 'json',
            success : function(response){        
              var distance = 0;
              $.each(response.data,function(key,value){          
                $(value).each(function( key,value ) {
                  if(distance === 0){
                    if(value.distanceInKM < 1){
                      distance = value.distanceInKM/0.0010000;
                    }else{
                      distance = value.distanceInKM;
                    }
                  }
                  if(value.distanceInKM < distance){
                    distance = value.distanceInKM;
                  } 
                  addMarker({ 
                    "latitude": value.latitude, 
                    "longitude": value.longitude,             
                    "title": value.name,
                    "id":value.id, 
                    "photoURL":value.photoURL, 
                    "distanceInKM":value.distanceInKM,
                    "country":value.country,
                    "state":value.state,
                    "city":value.city,
                    "postCode":value.postCode,
                    "availability":value.availability, 
                    "ratingCount":value.ratingCount,
                    "marker": null  
                  });
                });
              });
              if(response.data[0].length > 0){
                var distance_p = distance + 'km';
                if(distance < 0){
                  distance_p = (distance/0.0010000) + 'm';
                }
                $('#search_res_title').html('We found a dropd neighbour near you');
                $('#search_res_body').html('It’s just ' + distance_p + ' away from your location. Choose a neighbour and pick up your delivery from there house!');
                var click = '<button type="button" id="proceed_map">CHOOSE YOUR DROPD NEIGHBOUR</button>';
                $(click).insertAfter('#search_res_body');
      				// $('#map').show();
              }else{
                $('#search_res_title').html("I'm sorry.");
                $('#search_res_body').html("We are not able to find a neighbour within 10km from your address. Dropd currently serving Australia area.");
                $('#map').hide();
                $('#proceed_map').remove();
              }
            }
          });
          $(document).on('click', '#proceed_map', function(){
            $('.top-btn-list').css('display','block');
            $('#map').show();
            $('.mfp-close').trigger('click');
          });
          function addMarker(location) {
            // Create a new marker  
            location.marker = new google.maps.Marker( {
              position: new google.maps.LatLng(location.latitude, location.longitude),
              map: map,
              icon:"https://dropd.dynamicdreamz.com/DropDev/public/admin/images/dropd-location-marker.png",
              title: location.title  
            });
            const html = '<div class="gm-style-iw-d" style="overflow: scroll; max-height: 199px; id="'+location.id+'""><div>'
            //+'<div class="dropd_container">';
            +'<div class="dropd_row">'
            +'<div class="dropd_col_0">'
            +"<div class='dropd_profile' style='background-image: url("+location.photoURL+");'></div>"            
            +'</div>'
            +'<div class="dropd_col"><div class="dropd_title"><span>'+location.title+'</span></div>'
            +'<div class="dropd_rate"><i class="fa fa-star" aria-hidden="true"></i> <span class="dropd_rate_value">-</span><span class="dropd_rate_count">(No rating yet)</span></div></div>'
            +'<div class="dropd_col_0">'
            +'<div class="dropd_distance"><i class="fa fa-map-marker-alt" aria-hidden="true"></i> <span>'+location.distanceInKM+'Km</span></div></div>'
            +'</div>'
            +'<div class="dropd_row">'
            +'<div class="dropd_col"><div class="dropd_distance_mobile"><i class="fa fa-map-marker-alt" aria-hidden="true"></i><span>'+location.distanceInKM+'id="'+location.id+'"</span></div></div>'
            +'</div>'
            +'<div class="dropd_row">'
            +'<div class="dropd_col"><div class="dropd_details"><span>Availability: '+location.availability+'</span></div></div>'
            +'</div>'
            +'<div class="dropd_row">'
            +'<div class="dropd_col"><div class="dropd_cta"><button data-country="'+location.country+'" data-state="'+location.state+'" data-city="'+location.city+'" data-postcode="'+location.postCode+'" data-name="'+location.title+'" id="select_neighbour_btn_map" class="select_neighbour" data-id="'+location.id+'" store-name="Dropd" store-id="'+location.id+'" type="button">SELECT NEIGHBOUR & CHECK OUT</button></div></div>'
            +'</div>'
            +'</div>'
            +'</div>'
            +'</div>';
            const infowindow = new google.maps.InfoWindow({
              content: html,
            });
            map.setCenter(places[0].geometry.location);
        	map.setZoom(10);
            location.marker.addListener("click", () => {
             	$('.gm-ui-hover-effect').trigger('click');
            	location.marker.setIcon("https://dropd.dynamicdreamz.com/DropDev/public/admin/images/dropd-location-marker-selected.png");
            	infowindow.open(map, location.marker);
          	});
            google.maps.event.addListener(infowindow,'closeclick',function(){
              location.marker.setIcon("https://dropd.dynamicdreamz.com/DropDev/public/admin/images/dropd-location-marker.png");
            });
        }
        });
    });
}
function getListView(event) {
	$("#map").css("display","none");
	$("#list").css("display","block"); 
	var element = event.target.parentNode.parentNode;
	$(element).addClass('active');
	$(element).siblings().removeClass('active');
}
function getMapView(event) {
    $("#map").css("display","block"); 
    $("#list").css("display","none");
    var element = event.target.parentNode.parentNode;
    $(element).addClass('active');
    $(element).siblings().removeClass('active');
}
function getListViewData(lat,lng){
    $('#search_res_title').html('Fetching....');
    var img_loader = '<img src="https://dropd.dynamicdreamz.com/DropDev/public/admin/images/loader.gif" style="height:70px;">';
    $('#search_res_body').html(img_loader);
    $('#map').hide();
    $('#proceed_map').remove();
    $('.search_popup_link').trigger('click');
    radiusInKM = '10';
    $.ajax({
      	method : "POST",
      	// url : "https://apps.dynamicdreamz.com/DropDev/fetchNeighboursByLatLong",
      	url : "https://dropd.dynamicdreamz.com/DropDev/fetchNeighboursByLatLong",
      	data : {"latitude":lat,"longitude":lng,"radiusInKM":radiusInKM},
      	dataType : 'json',
      	success : function(response){
	        $('#list').html('');
	        result = response['neighbour_name'];
           $("#pac-input-1").autocomplete({
              source: result,
              minLength: 0
            }).focus(function() {          
              $(this).autocomplete('search', $(this).val());          
            });
	        $.each(response.data,function(key,value){          
	          	$(value).each(function( key,value ) {            
		            addhtml({ 
		              "title": value.name,
		              "id":value.id, 
		              "photoURL":value.photoURL, 
		              "distanceInKM":value.distanceInKM,
		              "country":value.country,
		              "state":value.state,
		              "city":value.city,
		              "postCode":value.postCode,
		              "availability":value.availability, 
		              "ratingCount":value.ratingCount,
		              "marker": null
		            });
	          	});
	        });
      	}
    });
}
function addhtml(res){
    htmladd = '<div class="list-group-item" id='+res.id+'>';
    htmladd +='<div class="dropd_row">';
    htmladd +="<div class='dropd_col_0'><div class='dropd_profile' style='background-image: url("+res.photoURL+")'></div></div>";
    htmladd +='<div class="dropd_col"><div class="dropd_title"><span>'+res.title+'</span></div>';
    htmladd +='<div class="dropd_rate"><i class="fa fa-star" aria-hidden="true"></i><span class="dropd_rate_value">-</span><span class="dropd_rate_count">(No rating yet)</span></div></div>';
    htmladd +='<div class="dropd_col_0"><div class="dropd_distance"><i class="fa fa-map-marker-alt" aria-hidden="true"></i> <span>'+res.distanceInKM+'Km</span></div></div></div>';
    htmladd +='<div class="dropd_row">';
    htmladd +='<div class="dropd_col"><div class="dropd_distance_mobile"><i class="fa fa-map-marker-alt" aria-hidden="true"></i> <span>'+res.distanceInKM+'Km</span></div></div></div>';
    htmladd +='<div class="dropd_row">';
    htmladd +='<div class="dropd_col"><div class="dropd_details"><span>Availability: '+res.availability+'</span></div></div>';
    htmladd +='<div class="dropd_col_0"><div class="dropd_cta"><button data-country="'+res.country+'" data-state="'+res.state+'" data-city="'+res.city+'" data-postcode="'+res.postCode+'" data-name="'+res.title+'" id="select_neighbour_btn_list" class="select_neighbour"  data-id="' + res.id +'" store-name="Dropd" store-id="' + res.id +'" type="button"><span>SELECT NEIGHBOUR & CHECK OUT</span><i class="fa fa-check" aria-hidden="true"></i></button></div></div></div></div>';
    $('#list').append(htmladd);  
}
$(document).on("click",'.select_neighbour',function(){
    var img_loader = '<div class="loader" style="display:none;position: absolute;top: 0;z-index: 9999;background: rgba(0,0,0,0.3);width: 100%;height: 100%;"><img src="https://dropd.dynamicdreamz.com/DropDev/public/admin/images/loader.gif" style="margin-left: 45%;margin-top: 15%;"></div>';
    $('body').append(img_loader);
    $('body').css('overflow','hidden');
    $('.loader').css('display','block');
    $.ajax({
      	method : "GET",
      	url : "https://dropd.dynamicdreamz.com/DropDev/get_productid?shop_url="+window.location.host,
	    success : function(response){
        	var result = JSON.parse(response)
	        if(result['success'] == "true"){         
	          var productId = result['product_id']//40055224860853;
	          getVariant(productId);
	        }else{
	          alert("Product doesn't created!..");
	        } 
	    }
    }); 
    function getVariant(productId){
      	$.ajax({
        	method : "GET",
        	url : "https://dropd.dynamicdreamz.com/DropDev/get_variantid?product_id="+productId+"&shop_url="+window.location.host,
	        success : function(response){
		        var result = JSON.parse(response);
		        if(result['success'] == "true"){          
		            var variantId = result['variant_id']//40055224860853;            
		            AddCartproduct(variantId);
		        }else{
		           alert("Product doesn't created!..");
		        } 
	        }
      	}); 
    }
   	var neighbour_id = $(this).data('id');
    var name = $(this).data('name');
    var country = $(this).data('country');
    var state = $(this).data('state');
    var city = $(this).data('city');
    var postCode = $(this).data('postcode');
    function AddCartproduct(variantId){
      	var params = {
          type: 'GET',
          url: '/cart.js',
          dataType: 'json',
          	success: function(response) {
	            $.each(response.items,function(key,value){
	              if(variantId == value.id){
	                updateCartAttributes(neighbour_id,name,country,state,city,postCode);
	              }else{
	                productAdded(variantId);
	              }
	            });
          	}
      	};
      	jQuery.ajax(params);
    }
    function productAdded(variantId){
      	var params = {
	        type: 'POST',
	        url: '/cart/add.js',
	        data: "id="+variantId+"&quantity=1",
	        dataType: 'json',
	        success: function(line_item) {
	          updateCartAttributes(neighbour_id,name,country,state,city,postCode);
	        },
	        error: function(XMLHttpRequest, textStatus) {
	        }
      	};
      	$.ajax(params);
    }
    function updateCartAttributes(neighbour_id,name,country,state,city,postCode) {
      var updateParams = {
        type: 'POST',
        url: '/cart/update.js',
        data: "attributes[DropID]="+neighbour_id+"&attributes[name]="+name+"&attributes[shopUrl]="+window.location.host+"&attributes[address1]=DROPD NEIGHBOUR SELECTED&attributes[city]="+city+"&attributes[country]="+country+"&attributes[province]="+state+"&attributes[zip]="+postCode,
        dataType: 'json',
        success: function(cart) {
          window.location.href = '/checkout?checkout[shipping_address][address1]= DROPD NEIGHBOUR SELECTED&checkout[shipping_address][city]='+city+'&checkout[shipping_address][country]='+country+'&checkout[shipping_address][province]='+state+'&checkout[shipping_address][zip]='+postCode;
        },
        error: function(XMLHttpRequest, textStatus) {
        }
      }; 
      $.ajax(updateParams);
    }
});
var params = {
    type: 'GET',
    url: '/cart.js',
    dataType: 'json',
    success: function(response) {
      var AddedDropid = response.attributes.DropID;
      if(AddedDropid != '' && AddedDropid != undefined){
        $(".cart__submit").before('<p>'+response.attributes.name+' Selected for Dropd service</p>');
        $('.cart__submit').css("display","none");
        $('.cart__submit').after('<a class="cart__submit btn btn--small-wide" id="cutsom_checkout" href="/checkout?checkout[shipping_address][address1]= Dropd address&checkout[shipping_address][city]='+response.attributes.city+'&checkout[shipping_address][country]='+response.attributes.country+'&checkout[shipping_address][province]='+response.attributes.province+'&checkout[shipping_address][zip]='+response.attributes.zip+'" style="margin-right: 15px;">Check out</a>');
        $('#cutsom_checkout').after('<a id="remove_btn" class="btn btn--small-wide">Remove</a>');
      }
    }
};
jQuery.ajax(params);
$(document).on("click",'#remove_btn', function(){
    $.ajax({
      method : "GET",
      url : "https://dropd.dynamicdreamz.com/DropDev/get_productid?shop_url="+window.location.host,
      success : function(response){
	        var result = JSON.parse(response)
	        if(result['success'] == "true"){
	          var productId = result['product_id']//40055224860853;result['variant_id'];;
	          getVariantRemove(productId);
	        }else{
	          alert("Product doesn't created!..");
	        } 
      	}
    });
    function getVariantRemove(productId){
	    $.ajax({
	        method : "GET",
	        url : "https://dropd.dynamicdreamz.com/DropDev/get_variantid?product_id="+productId+"&shop_url="+window.location.host,
	        success : function(response){
	          var result = JSON.parse(response)
	          if(result['success'] == "true"){
	            var variantId = result['variant_id']//40055224860853;            
	            //AddCartproduct(variant_id);
	            RemoveCartproduct(variantId);
	          }else{
	            alert("Product doesn't created!..");
	          } 
	        }
	    }); 
    }
    function RemoveCartproduct(variantId){
      var params = {
        type: 'POST',
        url: '/cart/change.js',
        data: "id="+variantId+"&quantity=0",
        dataType: 'json',
        success: function(line_item) {
          RemoveCartAttr();
        },
        error: function(XMLHttpRequest, textStatus) {
        }
      };
      jQuery.ajax(params);
    }
    function RemoveCartAttr(){
      var updateParams = {
        type: 'POST',
        url: '/cart/update.js',
        data: "attributes[DropID]=&attributes[name]=&attributes[shopUrl]=&attributes[address1]=&attributes[city]=&attributes[country]=&attributes[province]=&attributes[zip]=",
        dataType: 'json',
        success: function(cart) {
          location.reload();
        },
        error: function(XMLHttpRequest, textStatus) {
        }
      }; 
      $.ajax(updateParams);
    }
});

// document.write('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6etb4KPprWQK-v_JCUEH7M2N2eGj-t24&callback=initAutocomplete&libraries=places&v=weekly" type="text/javascript"></script>');