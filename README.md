# How to install APP on your server

## Server requirements:
- PHP version 7.3 or newer is required, with the *intl* extension and *mbstring* extension installed.

- The following PHP extensions should be enabled on your server: php-json, php-mysqlnd, php-xml

- In order to use the CURLRequest, you will need libcurl installed.

- A database is required for most web application programming. Currently supported databases are:

  MySQL (5.1+) via the MySQLi driver
  PostgreSQL via the Postgre driver
  SQLite3 via the SQLite3 driver
  MSSQL via the SQLSRV driver (version 2005 and above only)

- Not all of the drivers have been converted/rewritten for CodeIgniter4. The list below shows the outstanding ones.

  MySQL (5.1+) via the pdo driver
  Oracle via the oci8 and pdo drivers
  PostgreSQL via the pdo driver
  MSSQL via the pdo driver
  SQLite via the sqlite (version 2) and pdo drivers
  CUBRID via the cubrid and pdo drivers
  Interbase/Firebird via the ibase and pdo drivers
  ODBC via the odbc and pdo drivers (you should know that ODBC is actually an abstraction layer)


## Follow below steps:
- First install latest version of composer, if already exist then update composer.
- Install the project in Codeigniter 4 using this command: 

  composer create-project codeigniter4/framework DropDev

- Replace below directories on your project.
  ***
  1. app/
  2. public/

- Give permissions to writable directories.
  Use command :  chmod 777 -R /var/www/myproject/writable

- If env file is without dot(.), please rename it with .env
- Set belows important variable to .env files:
- Check example.env file for reference.
  ***
  * Set Base URL:
  - app.baseURL = '<base_url>'
  ***
  * Set Database connection details :
  - database.default.database = databasename
  - database.default.username = databaseusername
  - database.default.password = databasepassword
  ***
  * Set Recurring Charge API Config details
  - RECURRING_APP_CHARGE_NAME = 'store_charge'
  - RECURRING_APP_CHARGE_PRICE = 0.00
  - RECURRING_APP_CAPPED_AMOUNT = 1000
  - RECURRING_APP_TERMS = '$5 for each transaction'

  ***
  * Set script address for SCRIPT_TAG API:
  - SCRIPT_ADDRESS = <base_url>/public/jquery-2.2.4.min.js
  - SCRIPT_ADDRESS_2 = <base_url>/public/jquery.magnific-popup.min.js
  - SCRIPT_ADDRESS_3 = <base_url>/public/drop_custom_map.js


  ***
  * Set USAGE service charge:
  - USAGE_SERVICE_CHARGE = <05.00>
  
  ***
  * Set APP KEY, SECRET and REDIRECT URL:
  - API_KEY = <APP API_KEY>
  - API_SECRET = <APP API_SECRET>
  - REDIRECT = '<base_url>/redirect'
  
## Create database migrations:
- To create databasae migrations execute below command:

php spark migrate